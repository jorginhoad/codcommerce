<?php

namespace CodeCommerce\Http\Controllers;

use CodeCommerce\Category;
use CodeCommerce\Http\Requests;


class AdminCategoriesController extends Controller
{
    //
    private $category;

    public function __construct(Category $categories)
    {
        $this->category = $categories;
    }

    public function index()
    {
        $categories = $this->category->all();
        return view('categories', compact('categories'));
    }
}
