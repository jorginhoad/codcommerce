<?php

namespace CodeCommerce\Http\Controllers;

use CodeCommerce\Category;

use CodeCommerce\Http\Requests;


class ExemploController extends Controller
{
    //
    private $category;

    public function __construct(Category $categories){
         $this->category = $categories;
    }



    public function exemplo()
    {

        $categories = $this->category->all();

        return view('exemplo', compact('categories'));
    }

}
