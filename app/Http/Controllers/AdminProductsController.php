<?php

namespace CodeCommerce\Http\Controllers;

use CodeCommerce\Product;
use Illuminate\Http\Request;

use CodeCommerce\Http\Requests;
use CodeCommerce\Http\Controllers\Controller;

class AdminProductsController extends Controller
{
    //
    private $product;

    public function __construct(Product $products)
    {
        $this->product = $products;
    }


    public function index(){

        $products = $this->product->all();

        return view('products', compact('products'));
    }
}
