<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['prefix'=>'admin'], function()
{
    Route::get('categories',['as'=>'categories', 'uses'=>'AdminCategoriesController@index']);
    Route::get('products',['as'=>'products', 'uses'=>'AdminProductsController@index']);

});


Route::get('/', function () {
    return view('welcome');
});

